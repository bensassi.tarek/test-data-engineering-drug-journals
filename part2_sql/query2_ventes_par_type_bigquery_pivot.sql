WITH 

T0 AS (
  SELECT 
    T.client_id,
    PN.product_type,
    SUM(prod_qty * prod_price) AS total_price
  FROM 
    `project_id.dataset_id.transaction` T,
    `project_id.dataset_id.product_nomenclature` PN
  WHERE 
    T.prod_id = PN.product_id
    AND T.date BETWEEN '2020-01-01' AND '2020-12-31'
  GROUP BY T.client_id, PN.product_type
)

SELECT 
  client_id,
  MEUBLE AS ventes_meuble,
  DECO AS ventes_deco
FROM (
  SELECT *
  FROM T0
  PIVOT(SUM(total_price) FOR product_type in ('DECO', 'MEUBLE'))
)