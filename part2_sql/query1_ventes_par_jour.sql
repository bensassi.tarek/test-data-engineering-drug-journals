SELECT 
	date, 
	SUM(prod_qty * prod_price) AS ventes
FROM `project_id.dataset_id.transaction`
WHERE date BETWEEN '2019-01-01' AND '2019-12-31'
GROUP BY date
ORDER BY date