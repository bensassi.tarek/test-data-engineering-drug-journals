SELECT
  client_id,
  SUM(CASE WHEN PN.product_type = 'MEUBLE' THEN T.prod_price * T.prod_qty ELSE 0 END) AS ventes_meuble,
  SUM(CASE WHEN PN.product_type = 'DECO' THEN T.prod_price * T.prod_qty ELSE 0 END) AS ventes_deco
FROM `project_id.dataset_id.transaction` T
JOIN `project_id.dataset_id.product_nomenclature` PN 
ON T.prod_id = PN.product_id
WHERE T.date BETWEEN '2020-01-01' AND '2020-12-31'
GROUP BY client_id