import logging

def configure_logging(log_level=logging.INFO, log_file=None):
    """
    Configure the logging settings.
    """
    logging.basicConfig(level=log_level,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        filename=log_file,
                        filemode='a')
    
    root_logger = logging.getLogger('')
    
    # Check if a StreamHandler already exists for the root logger
    if not any(isinstance(handler, logging.StreamHandler) for handler in root_logger.handlers):
        console = logging.StreamHandler()
        console.setLevel(log_level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        console.setFormatter(formatter)
        root_logger.addHandler(console)


def get_logger(name):
    """
    Get a logger instance.
    """
    return logging.getLogger(name)