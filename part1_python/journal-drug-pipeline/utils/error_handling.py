import logging
from utils.logging_utils import get_logger

logger = get_logger(__name__)

class PipelineError(Exception):
    """
    Custom exception for pipeline errors.
    """
    def __init__(self, message):
        super().__init__(message)
        logger.error(message)

def handle_error(error_message, raise_exception=True):
    """
    Handle errors: log them and optionally raise an exception.
    """
    logger.error(error_message)
    if raise_exception:
        raise PipelineError(error_message)