from config.settings import *

from components.extract.gcs.io import *

from components.extract.gcs.manage_gcs_files import move_gcs_files
from components.extract.gcs.manage_gcs_files import delete_gcs_files
from components.extract.gcs.convert_json_to_csv import convert_json_to_csv
from components.extract.gcs.validate_or_fix_json import validate_or_fix_json
from components.extract.gcs.validate_or_fix_csv import validate_or_fix_csv

from components.clean.clean_data import clean_data
from components.validate.validate_cleaned_data import validate_cleaned_data
from components.transform_and_load.merge_raw_table import find_and_merge_drug_mentions_raw_enriched
from components.transform_and_load.drug_mentions_json import load_drug_mentions_json

if __name__ == "__main__":
    
    # Component 1: Move GCS files to 'processing' folder
    input_path = input_data_path
    output_path = processing_path
    move_gcs_files(bucket_name, input_path, output_path)

    # Component 2: Validate or Fix JSON files
    input_path = processing_path
    output_path = valid_json_files_path
    validate_or_fix_json(bucket_name, input_path, output_path, unhandled_path)

    # Component 3: Convert JSON files to CSV
    input_path = valid_json_files_path
    output_path = not_cleaned_path
    original_path = processing_path
    convert_json_to_csv(bucket_name, input_path, output_path, unhandled_path, original_path)

    # Component 4: Validate or Fix CSV files
    input_path = processing_path
    output_path = not_cleaned_path
    validate_or_fix_csv(bucket_name, input_path, output_path, unhandled_path, expected_columns_list)

    # Component 5: Clean data
    input_path = not_cleaned_path
    output_path = cleaned_path
    clean_data(bucket_name, input_path, output_path)

    # Component 6: Validate cleaned data
    input_path = cleaned_path
    output_path = validated_path
    validate_cleaned_data(bucket_name, input_path, output_path, column_list, column_required, column_categories, date_format, bad_patterns)

    # Component 7: Transform (1) and Load to BigQuery: Merge table drug_mentions_raw_enriched
    input_path = validated_path
    find_and_merge_drug_mentions_raw_enriched(drug_blob_name, input_path, bucket_name, project_id, dataset_id, table_name)

    # Component 8: Transform (2) and load to GCS: drug_mentions json
    output_path = output_data_path
    load_drug_mentions_json(bucket_name, output_path, final_name, project_id, dataset_id, table_name, article_type_dict, indent=4)

    # Component 9: Move GCS files to 'archived'
    input_path = processing_path
    output_path = archived_path
    move_gcs_files(bucket_name, input_path, output_path)

    # Component 10: Delete GCS tmp files
    input_path = tmp_path
    delete_gcs_files(bucket_name, input_path)