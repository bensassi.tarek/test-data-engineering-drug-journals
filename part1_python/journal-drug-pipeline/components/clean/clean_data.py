import pandas as pd
import numpy as np
import re
from datetime import datetime

from components.extract.gcs.io import *

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def reformat_date(date_str):
    """Convert various date formats into 'yyyy-mm-dd'."""
    
    # If the date is in the 'yyyy-mm-dd' format, return it
    if re.match(r'\d{4}-\d{2}-\d{2}', date_str):
        return date_str

    # If the date is in the 'dd/mm/yyyy' format, convert it to 'yyyy-mm-dd'
    if re.match(r'\d{2}/\d{2}/\d{4}', date_str):
        day, month, year = date_str.split('/')
        return f"{year}-{month}-{day}"

    # Otherwise, try converting from the format 'd Month yyyy'
    try:
        date_object = datetime.strptime(date_str, '%d %B %Y')
        return date_object.strftime('%Y-%m-%d')
    except ValueError:
        # log value for debugging
        return ''  # If the conversion fails, return an empty string

def remove_bad_patterns(data):
    # Remove \xc3\xb1 and \xc3\x28 patterns using regex
    return re.sub(r'\\.{3}\\.{3}', '', data)

def clean_data_blob(bucket_name, blob_name, output_path):
    if '.csv' in blob_name:
        filename = get_filename(blob_name)

        # Force id type to str
        data_types = {'id': str}
        df = pd.read_csv(f"gs://{bucket_name}/{blob_name}", dtype=data_types)

        # Replace NaN values by an empty string
        df = df.replace({np.nan: ''})

        if 'scientific_title' in set(df.columns):
            df['article_type'] = 'scientific_title'
            # Rename column scientific_title (if exists) to title
            df = df.rename(columns={'scientific_title': 'title'})
        else:
            df['article_type'] = 'title'

        # Remove \xc3\xb1 and \xc3\x28 patterns using regex
        df['title'] = df['title'].apply(remove_bad_patterns)
        df['journal'] = df['journal'].apply(remove_bad_patterns)

        # Filter rows having title in (null values, empty strings, strings made up solely of spaces)
        df = df[df['title'].str.strip() != '']

        # Convert various date formats into 'yyyy-mm-dd'
        df['date'] = df['date'].apply(reformat_date)
        # Delete bad dates after logging the value for manual check
        df = df[df['date'] != '']
 
        if not(df.empty):
            content = df.to_csv(index=False)
            write_blob_csv(bucket_name, f"{output_path}/{filename}", content)
            logger.info(f"{filename} CSV file has been cleaned.")

def clean_data(bucket_name, input_path, output_path):
    blobs = list_blobs(bucket_name, input_path, keyword='.csv')
    logger.info("Starting the data cleaning...")

    for blob_name in blobs:
        clean_data_blob(bucket_name, blob_name, output_path) 