import pandas as pd
import numpy as np
from collections import defaultdict
import json

from components.extract.gcs.io import *

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def drug_mentions_json(df, article_type_dict):
    # Replace NaN values by an empty string
    df = df.replace({np.nan: ''})

    grouped = df.groupby('atccode')

    output = defaultdict(lambda: {"drug": None, "pubmed": [], "clinical_trial": [], "journal": []})

    for atccode, group in grouped:
        output[atccode]["drug"] = group["drug"].iloc[0]  # Assuming drug is unique per atccode

        for article_type in article_type_dict.keys():
            filtered_group = group[group["article_type"] == article_type]

            mentions = []
            seen = set()  # To keep track of duplicates
            
            for idx, row in filtered_group.iterrows():
                mention = {
                    "id": row["id"],
                    "date": row["date"].strftime('%Y-%m-%d'),
                    "title": row["title"],
                    "journal": row["journal"]
                }

                mention_tuple = (mention["date"], mention["id"], mention["title"], mention["journal"])
                if mention_tuple not in seen:
                    mentions.append(mention)
                    seen.add(mention_tuple)
            
            article_type_name = article_type_dict[article_type]
            output[atccode][article_type_name] = mentions

        journals = [{"date": row["date"].strftime('%Y-%m-%d'), "journal": row["journal"]} for idx, row in group.iterrows()]
        # Deduplicate journals based on date and journal name
        journals = list({(j["date"], j["journal"]): j for j in journals}.values())
        
        output[atccode]["journal"] = journals

    # Convert defaultdict to regular dict
    return dict(output)

def load_drug_mentions_json(bucket_name, output_path, final_name, project_id, dataset_id, table_name, article_type_dict, indent=4):
    logger.info("Starting Transform (2) and load to GCS: drug_mentions json...")   

    # Use the read_gbq function to fetch the data and load into a DataFrame
    table = f"{project_id}.{dataset_id}.{table_name}"
    query = f"SELECT * FROM `{table}`"
    df = pd.read_gbq(query, project_id=project_id, dialect='standard')

    output = drug_mentions_json(df, article_type_dict)
    
    final_json = json.dumps(output, indent=indent, ensure_ascii=False)

    write_blob_json(bucket_name, f"{output_path}/{final_name}", final_json.encode('utf-8'))