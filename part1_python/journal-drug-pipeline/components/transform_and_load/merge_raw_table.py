import pandas as pd
from pandas_gbq import to_gbq, read_gbq
from google.cloud import bigquery
import datetime
import uuid

from components.extract.gcs.io import *

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def generate_unique_table_name(prefix="tmp_table_"):
    # Get the current timestamp
    timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
    
    # Generate a random UUID
    random_str = uuid.uuid4().hex
    
    # Combine the timestamp and UUID to form the unique table name
    table_name = f"{prefix}{timestamp}_{random_str}"
    
    return table_name

def find_drug(df_drug, df_article):
        # List to collect rows with drug mentions
        rows = []

        # Iterate over each drug
        for index, row in df_drug.iterrows():
            drug = row['drug']
            
            # Filter df_article where the title contains the drug
            mask = df_article['title'].str.contains(drug, case=False, na=False)
            matched_articles = df_article[mask].copy()
            
            # Add the 'atccode' and 'drug' columns to matched articles
            matched_articles['atccode'] = row['atccode']
            matched_articles['drug'] = drug
            
            rows.append(matched_articles)

        # Combine all matched articles
        df_mentions = pd.concat(rows)

        # Reorder columns
        df_mentions = df_mentions[['atccode', 'drug', 'article_type', 'date', 'id', 'title', 'journal']]

        if not(df_mentions.empty):
            return True, df_mentions
        else:
            return False, None
    
def merge_drug_mentions_raw_enriched(drug_blob_name, article_blob_name, bucket_name, project_id, dataset_id, table_name):
    if '.csv' in drug_blob_name and '.csv' in article_blob_name:
        df_drug = pd.read_csv(f"gs://{bucket_name}/{drug_blob_name}")
        df_article = pd.read_csv(f"gs://{bucket_name}/{article_blob_name}", dtype={'id':str})
        not_empty, df_mentions = find_drug(df_drug, df_article)
        
        if not_empty:
            # Connect to BigQuery project
            client = bigquery.Client(project=project_id)

            table = f"{project_id}.{dataset_id}.{table_name}"

            # Write DataFrame to a temporary table
            # use a combination of the current timestamp and a random string. 
            # This approach ensures that even if multiple tables are created at the same time, they will have unique names.
            tmp_table_name = generate_unique_table_name(prefix="tmp_table_")
            tmp_table = f"{project_id}.{dataset_id}.{tmp_table_name}"
            # Define the schema to ensure that the date column is stored as date in the tmp table
            schema = [{'name': 'date', 'type': 'DATE'}]
            to_gbq(df_mentions, tmp_table, project_id=project_id, if_exists='replace', table_schema=schema, progress_bar=False)

            merge_sql = f"""
            MERGE {table} AS target
            USING {tmp_table} AS source
            ON target.atccode = source.atccode 
                AND target.article_type = source.article_type
                AND target.date = source.date
                AND target.title = source.title
                AND (target.id is null OR target.id = source.id)
                AND (target.journal is null OR target.journal = source.journal)

            WHEN MATCHED THEN
                UPDATE SET 
                    target.drug = source.drug,
                    target.id = source.id,
                    target.journal = source.journal
            WHEN NOT MATCHED THEN
                INSERT (atccode, drug, article_type, date, id, title, journal)
                VALUES (source.atccode, source.drug, source.article_type, source.date, source.id, source.title, source.journal);
            """

            # Execute the SQL Merge statement
            read_gbq(merge_sql, project_id=project_id)

            # Drop the temporary table
            client.delete_table(tmp_table)

def find_and_merge_drug_mentions_raw_enriched(drug_blob_name, input_path, bucket_name, project_id, dataset_id, table_name):
    blobs = list_blobs(bucket_name, input_path, keyword='.csv')
    logger.info("Starting Transform (1) and Load to BigQuery: Merge table drug_mentions_raw_enriched...")

    for article_blob_name in blobs:
        merge_drug_mentions_raw_enriched(drug_blob_name, article_blob_name, bucket_name, project_id, dataset_id, table_name)