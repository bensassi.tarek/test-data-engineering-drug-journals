import pandas as pd
import re

from components.extract.gcs.manage_gcs_files import *
from components.extract.gcs.io import *

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def is_valid_date_format(df, column_name, format):
    try:
        # Convert the column using the desired format
        pd.to_datetime(df[column_name], format=format, errors='raise')
        return True
    except Exception as e:
        return False

def validate_cleaned_data_blob(bucket_name, blob_name, output_path, column_list=None, column_required=None, column_categories=None, date_format=None, bad_patterns=None):
    if '.csv' in blob_name:
        filename = get_filename(blob_name)

        try:
            df = pd.read_csv(f"gs://{bucket_name}/{blob_name}")

            if column_list and set(df.columns) != set(column_list):
                raise ValueError(f"Bad column list {list(df.columns)}")
            
            if column_required:
                for column in column_required:
                    all_non_empty = (df[column].notna() & (df[column].str.strip() != '')).all()
                    if not(all_non_empty.any()):
                        raise ValueError(f"{column} column is required")

            if column_categories:
                for key, value in column_categories.items():
                    expected_categories = set(value)
                    unique_categories = set(df[key].unique())
                    if not(unique_categories.issubset(expected_categories)):
                        raise ValueError(f"{key} categorical column has at least one bad value {unique_categories} not in the expected categories {expected_categories}")

            if date_format:
                for key, value in date_format.items():
                    if not(is_valid_date_format(df, key, value)):
                        raise ValueError(f"{key} column has a bad format {value}")

            if bad_patterns:
                for key, value in bad_patterns.items():
                    for pattern in value:
                        contains_pattern = df[key].apply(lambda x: bool(re.search(pattern, str(x))))
                        if contains_pattern.any():
                            raise ValueError(f"{key} str column has a bad pattern {pattern}")


            copy_blob(bucket_name, blob_name, bucket_name, f"{output_path}/{filename}")
            logger.info(f"{filename} is valid and ready to be processed.")
        except ValueError as e:
            # Move to unhandled folder to check manually
            # move_blob(bucket_name, blob_name, bucket_name, f"{unhandled_path}/{filename}")
            logger.warning(f"{filename} is not valid. Error: {e}")

def validate_cleaned_data(bucket_name, input_path, output_path, column_list, column_required, column_categories, date_format, bad_patterns):
    blobs = list_blobs(bucket_name, input_path, keyword='.csv')
    logger.info("Starting the Validate cleaned data component...")

    for blob_name in blobs:
        validate_cleaned_data_blob(bucket_name, blob_name, output_path, column_list, column_required, column_categories, date_format, bad_patterns)