import json
import csv
from io import StringIO

from components.extract.gcs.manage_gcs_files import *
from components.extract.gcs.io import *

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def json_string_to_csv_string(json_str):
    # Parse the JSON data
    data = json.loads(json_str)
    
    # Convert to CSV format
    output = StringIO()
    
    # Check if the JSON data is a list of dictionaries
    if isinstance(data, list) and isinstance(data[0], dict):
        keys = data[0].keys()
        writer = csv.DictWriter(output, fieldnames=keys)
        writer.writeheader()
        for row in data:
            if len(row):
                writer.writerow(row)
        return output.getvalue()
    else:
        raise ValueError("The JSON string does not have the expected structure (list of dictionaries).")

def convert_json_to_csv_blob(bucket_name, blob_name, output_path, unhandled_path, original_path):
    if '.json' in blob_name:
        json_filename = get_filename(blob_name)
        csv_filename = 'converted_'+json_filename.replace('.json', '.csv')
        json_str = read_blob(bucket_name, blob_name)

        try:
            csv_str = json_string_to_csv_string(json_str)
            write_blob_csv(bucket_name, f"{output_path}/{csv_filename}", csv_str)
            logger.info(f"{json_filename} JSON file has been converted to CSV.")
        except Exception as e:
            # Move to unhandled folder to check manually
            move_blob(bucket_name, f"{original_path}/{json_filename}", bucket_name, f"{unhandled_path}/{json_filename}")
            logger.warning(f"{json_filename} JSON file cannot be converted to CSV.")

def convert_json_to_csv(bucket_name, input_path, output_path, unhandled_path, original_path):
    blobs = list_blobs(bucket_name, input_path, keyword='.json')
    logger.info("Starting the component Convert json to CSV...")
    
    for blob_name in blobs:
        convert_json_to_csv_blob(bucket_name, blob_name, output_path, unhandled_path, original_path)