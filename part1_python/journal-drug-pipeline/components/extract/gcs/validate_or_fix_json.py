import json

from components.extract.gcs.manage_gcs_files import *
from components.extract.gcs.io import *

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def fix_json(data):
    # Remove trailing commas
    data = data.replace("},\n]", "}]")
    
    return data

def validate_or_fix_json_blob(bucket_name, blob_name, output_path, unhandled_path):
    if '.json' in blob_name:
        filename = get_filename(blob_name)
        content = read_blob(bucket_name, blob_name)

        try:
            # Try to load the JSON to see if it's valid
            parsed = json.loads(content)
            copy_blob(bucket_name, blob_name, bucket_name, f"{output_path}/{filename}")
            logger.info(f"{filename} JSON file is valid.")
        except json.JSONDecodeError:
            # If an error occurs, apply the fix
            content = fix_json(content)

            # Try loading the JSON again to check the fix
            try:
                parsed = json.loads(content)
                write_blob_json_list(bucket_name, f"{output_path}/{filename}", parsed)
                logger.info(f"{filename} JSON file has been fixed.")
            except json.JSONDecodeError:
                # Move to unhandled folder to check manually
                move_blob(bucket_name, blob_name, bucket_name, f"{unhandled_path}/{filename}")
                logger.warning(f"{filename} JSON file cannot be fixed.")

def validate_or_fix_json(bucket_name, input_path, output_path, unhandled_path):
    blobs = list_blobs(bucket_name, input_path, keyword='.json')
    logger.info("Starting the component Validate or Fix CSV...")

    for blob_name in blobs:
        validate_or_fix_json_blob(bucket_name, blob_name, output_path, unhandled_path)