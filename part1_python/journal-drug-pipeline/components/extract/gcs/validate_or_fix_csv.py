import csv
from io import StringIO

from components.extract.gcs.manage_gcs_files import *
from components.extract.gcs.io import *

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def is_valid_csv(csv_string, expected_columns_list):
    try:
        # Use StringIO to convert the string to a file-like object so we can read from it
        csv_file = StringIO(csv_string)
        
        # Use the csv library to read the file. The csv.reader by default considers double quotes
        # and will split the string correctly even if fields are enclosed in double quotes.
        reader = csv.reader(csv_file)
        
        # Convert the reader to a list
        rows = list(reader)

        # Check if header matches any of the expected column sets
        if set(rows[0]) not in expected_columns_list:
            return False

        # Get the number of fields in the header
        header_number = len(rows[1])
        
        # Check each row
        for row in rows[1:]:
            if len(row) != header_number:
                return False

        return True
    except csv.Error:
        return False
        
def fix_csv(data):
    # Remove the leading and trailing newlines.
    data = data.strip()

    return data

def validate_or_fix_csv_blob(bucket_name, blob_name, output_path, unhandled_path, expected_columns_list):
    if '.csv' in blob_name:
        filename = get_filename(blob_name)
        content = read_blob(bucket_name, blob_name)

        # Check
        if is_valid_csv(content, expected_columns_list):
            copy_blob(bucket_name, blob_name, bucket_name, f"{output_path}/{filename}")
            logger.info(f"{filename} CSV file is valid.")
        else:
            # Apply the fix
            content = fix_csv(content)
            if is_valid_csv(content, expected_columns_list):
                write_blob_csv(bucket_name, f"{output_path}/{filename}", content)
                logger.info(f"{filename} CSV file has been fixed.")
            else:
                # Move to unhandled folder to check manually
                move_blob(bucket_name, blob_name, bucket_name, f"{unhandled_path}/{filename}")
                logger.warning(f"{filename} CSV file cannot be fixed.")

def validate_or_fix_csv(bucket_name, input_path, output_path, unhandled_path, expected_columns_list):
    blobs = list_blobs(bucket_name, input_path, keyword='.csv')
    logger.info("Starting the component Validate or Fix CSV...")

    for blob_name in blobs:
        validate_or_fix_csv_blob(bucket_name, blob_name, output_path, unhandled_path, expected_columns_list)