from components.extract.gcs.io import *
from google.cloud import storage

from utils.logging_utils import configure_logging, get_logger
from utils.error_handling import handle_error

# Configure logging
configure_logging()

# Get a logger instance for the pipeline
logger = get_logger("DataPipeline")

def copy_blob(bucket_name, blob_name, destination_bucket_name, destination_blob_name):
    """Copies a blob from one bucket to another with a new name."""

    storage_client = storage.Client()

    source_bucket = storage_client.bucket(bucket_name)
    source_blob = source_bucket.blob(blob_name)
    destination_bucket = storage_client.bucket(destination_bucket_name)

    source_bucket.copy_blob(
        source_blob, destination_bucket, destination_blob_name,
    )

def delete_blob(bucket_name, blob_name):
    """Deletes a blob from the bucket."""

    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)
    generation_match_precondition = None

    # Optional: set a generation-match precondition to avoid potential race conditions
    # and data corruptions. The request to delete is aborted if the object's
    # generation number does not match your precondition.
    blob.reload()  # Fetch blob metadata to use in generation_match_precondition.
    generation_match_precondition = blob.generation

    blob.delete(if_generation_match=generation_match_precondition)

def move_blob(bucket_name, blob_name, destination_bucket_name, destination_blob_name):
    """Moves a blob from one bucket to another with a new name."""

    storage_client = storage.Client()

    source_bucket = storage_client.bucket(bucket_name)
    source_blob = source_bucket.blob(blob_name)
    destination_bucket = storage_client.bucket(destination_bucket_name)

    source_bucket.copy_blob(
        source_blob, destination_bucket, destination_blob_name,
    )
    source_bucket.delete_blob(blob_name)

def delete_gcs_files(bucket_name, input_path):
    blobs = list_blobs(bucket_name, input_path)
    logger.info(f"Starting the component Delete GCS {input_path} files...")

    for blob_name in blobs:
        delete_blob(bucket_name, blob_name)

def move_gcs_files(bucket_name, input_path, output_path):
    blobs = list_blobs(bucket_name, input_path)
    logger.info(f"Starting the component Move GCS files to {output_path}...")

    for blob_name in blobs:
        filename = get_filename(blob_name)
        move_blob(bucket_name, blob_name, bucket_name, f"{output_path}/{filename}")