# test_find_drug.py

import pytest
import pandas as pd
from components.transform_and_load.merge_raw_table import find_drug  

def test_find_drug_function():
    # Mock input data
    drug_data = {
        'atccode': ['A01'],
        'drug': ['DrugA']
    }
    article_data = {
        'article_type': ['pubmed'],
        'date': ['2021-01-01'],
        'id': ['1'],
        'title': ['Title mentioning DrugA'],
        'journal': ['Journal1']
    }
    
    df_drug = pd.DataFrame(drug_data)
    df_article = pd.DataFrame(article_data)

    # Call the function
    result_exists, result_df = find_drug(df_drug, df_article)

    # Define the expected output
    expected_df = pd.DataFrame({
        'atccode': ['A01'],
        'drug': ['DrugA'],
        'article_type': ['pubmed'],
        'date': ['2021-01-01'],
        'id': ['1'],
        'title': ['Title mentioning DrugA'],
        'journal': ['Journal1']
    })

    # Assert that the result matches the expected output
    assert result_exists == True
    assert result_df.equals(expected_df)