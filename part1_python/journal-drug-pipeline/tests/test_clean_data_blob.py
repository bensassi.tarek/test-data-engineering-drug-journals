# test_clean_data_blob.py

import pytest
import pandas as pd
from unittest.mock import patch, Mock
from components.clean.clean_data import clean_data_blob

@pytest.fixture
def mock_read_csv():
    """Mock pd.read_csv to return a predefined DataFrame."""
    with patch('components.clean.clean_data.pd.read_csv') as mock:
        yield mock

@pytest.fixture
def mock_write_blob_csv():
    """Mock write_blob_csv to avoid actual writing."""
    with patch('components.clean.clean_data.write_blob_csv') as mock: 
        yield mock

def test_clean_data_blob_function(mock_read_csv, mock_write_blob_csv):
    # Mock input data
    input_data = {
        'id': ['1'],
        'scientific_title': ['Title with \\xc3\\xb1pattern'],
        'journal': ['Journal with\\xc3\\x28 pattern'],
        'date': ['01/01/2021']
    }
    mock_read_csv.return_value = pd.DataFrame(input_data)

    # Define the input parameters
    bucket_name = 'journal-drug'
    blob_name = 'input.csv'
    output_path = 'output_path'

    # Call the function
    clean_data_blob(bucket_name, blob_name, output_path)

    # Check if write_blob_csv was called with the expected arguments
    expected_content = 'id,title,journal,date,article_type\r\n1,Title with pattern,Journal with pattern,2021-01-01,scientific_title\r\n'
    mock_write_blob_csv.assert_called_once_with(bucket_name, f"{output_path}/input.csv", expected_content)