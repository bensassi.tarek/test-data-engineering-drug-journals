# test_drug_mentions_json.py

import pytest
import pandas as pd
from collections import defaultdict
from components.transform_and_load.drug_mentions_json import drug_mentions_json

def test_drug_mentions_json_function():
    # Mock input data
    data = {
        'atccode': ['A01AD', 'A01AD', 'A04AD'],
        'drug': ['EPINEPHRINE', 'EPINEPHRINE', 'DIPHENHYDRAMINE'],
        'article_type': ['title', 'scientific_title', 'scientific_title'],
        'id': ["7", "NCT04188184", "NCT04237091"],
        'date': [pd.Timestamp('2020-02-01'), pd.Timestamp('2020-04-27'), pd.Timestamp('2020-01-01')],
        'title': ['The High Cost of Epinephrine Autoinjectors and Possible Alternatives.', 'Tranexamic Acid Versus Epinephrine During Exploratory Tympanotomy', 'Feasibility of a Randomized Controlled Clinical Trial Comparing the Use of Cetirizine to Replace Diphenhydramine in the Prevention of Reactions Related to Paclitaxel'],
        'journal': ['The journal of allergy and clinical immunology', 'Journal of emergency nursing', 'Journal of emergency nursing']
    }
    
    df = pd.DataFrame(data)

    article_type_dict = {
        'title': 'pubmed', 
        'scientific_title': 'clinical_trial'
    }

    # Call the function
    result = drug_mentions_json(df, article_type_dict)

    # Define the expected output
    expected_output = {
        'A01AD': {
            'drug': 'EPINEPHRINE',
            'pubmed': [{'id': "7", 'date': '2020-02-01', 'title': 'The High Cost of Epinephrine Autoinjectors and Possible Alternatives.', 'journal': 'The journal of allergy and clinical immunology'}],
            'clinical_trial': [{'id': "NCT04188184", 'date': '2020-04-27', 'title': 'Tranexamic Acid Versus Epinephrine During Exploratory Tympanotomy', 'journal': 'Journal of emergency nursing'}],
            'journal': [{'date': '2020-02-01', 'journal': 'The journal of allergy and clinical immunology'}, {'date': '2020-04-27', 'journal': 'Journal of emergency nursing'}]
        },
        'A04AD': {
            'drug': 'DIPHENHYDRAMINE',
            'pubmed': [],
            'clinical_trial': [{'id': "NCT04237091", 'date': '2020-01-01', 'title': 'Feasibility of a Randomized Controlled Clinical Trial Comparing the Use of Cetirizine to Replace Diphenhydramine in the Prevention of Reactions Related to Paclitaxel', 'journal': 'Journal of emergency nursing'}],
            'journal': [{'date': '2020-01-01', 'journal': 'Journal of emergency nursing'}]
        }
    }

    # Assert that the result matches the expected output
    assert result == expected_output