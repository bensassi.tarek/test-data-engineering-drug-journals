from datetime import datetime

pieline_name = 'journal-drug'
timestamp_str = datetime.now().strftime("%Y%m%d%H%M%S")
pipeline_path = f'{pieline_name}-{timestamp_str}'

project_id = 'tbs-2021'
dataset_id = 'drug_journal'
bucket_name = 'journal-drug'

# bucket persisted folders

ref_data_path = 'ref_data'
input_data_path = 'input_data'
processing_path = 'processing'
unhandled_path = 'unhandled'
archived_path = 'archived'
output_data_path = f'output_data'

# bucket tmp folders

tmp_path = f'tmp/{pipeline_path}'
valid_json_files_path = f'{tmp_path}/valid_json_files'
converted_json_files_path = f'{tmp_path}/converted_json_files'
not_cleaned_path = f'{tmp_path}/not_cleaned'
cleaned_path = f'{tmp_path}/cleaned'
validated_path = f'{tmp_path}/validated'

# validate_or_fix_json

expected_columns_list = [
    {"id", "title", "date", "journal"}, 
    {"id", "scientific_title", "date", "journal"}
    ]

# validate_cleaned_data

date_format_str = '%Y-%m-%d'
column_list = ['id', 'article_type', 'date', 'title', 'journal']
column_required = ['article_type', 'date', 'title']
column_categories = {'article_type': ['title', 'scientific_title']}
date_format = {'date': date_format_str}
bad_patterns = {
        'title': [r'\\.{3}\\.{3}'],
        'journal': [r'\\.{3}\\.{3}']
        }

# transform1: merge_raw_table_drug_mentions

drug_blob_name = 'ref_data/drugs.csv'
table_name = 'drug_mentions_raw_enriched'

# transform2: drug_mentions_json

article_type_dict = {
        'title': 'pubmed', 
        'scientific_title': 'clinical_trial'
}

final_name = 'drug_mentions.json'