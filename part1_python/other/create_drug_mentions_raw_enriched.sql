CREATE TABLE `{project_id}.{dataset_id}.drug_mentions_raw_enriched`
(
  atccode STRING NOT NULL,
  drug STRING NOT NULL,
  article_type STRING NOT NULL,
  date DATE NOT NULL,
  id STRING,
  title STRING NOT NULL,
  journal STRING
)
CLUSTER BY atccode;