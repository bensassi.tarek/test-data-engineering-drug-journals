from google.cloud import storage
import json

def read_blob(bucket_name, blob_name):
    """Write and read a blob from GCS using file-like IO"""
    # The ID of your GCS bucket
    # bucket_name = "your-bucket-name"

    # The ID of your new GCS object
    # blob_name = "storage-object-name"

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)

    # Mode can be specified as wb/rb for bytes mode.
    # See: https://docs.python.org/3/library/io.html
    with blob.open("r") as f:
        return f.read()

bucket_name = 'journal-drug'
blob_name = 'output_data/drug_mentions.json'
content = read_blob(bucket_name, blob_name)

# Load the JSON
data = json.loads(content)

# Dictionary to store the count of drugs for each journal
journal_drug_count = {}

# Iterate through each entry in the JSON
for key, value in data.items():
    for journal_entry in value['journal']:
        journal_name = journal_entry['journal']
        if journal_name not in journal_drug_count:
            journal_drug_count[journal_name] = set()
        journal_drug_count[journal_name].add(value['drug'])

# Convert the sets to their size to get the number of different drugs
for key, value in journal_drug_count.items():
    journal_drug_count[key] = len(value)

# Find the maximum number of drug mentions
max_count = max(journal_drug_count.values())

# Find all journals with this maximum number
max_journals = [journal for journal, count in journal_drug_count.items() if count == max_count]

print(f"The journals {', '.join(max_journals)} mention the most different drugs, with {max_count} drugs each.")